function quickSort(array) {
  if (array === undefined || array.length <= 1) {
    return array;
  }

  let left = [];
  let right = [];

  const pivot = array.pop();

  for (let i = 0; i < array.length; i++) {
    if (array[i] <= pivot) {
      left.push(array[i]);
    } else {
      right.push(array[i]);
    }
  }

  return [].concat(quickSort(left), pivot, quickSort(right));
}

module.exports.quickSort = quickSort;
