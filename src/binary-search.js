function binarySearch(array, findValue, start, end) {
  if (start === undefined) {
    start = 0;
  }

  if (end === undefined) {
    end = array.length - 1;
  }

  if (start > end) {
    return -1;
  }

  if (array[0] === findValue) {
    return 0;
  }

  if (array[end] === findValue) {
    return end;
  }

  const middle = Math.floor(start + (end - start) / 2);

  if (array[middle] === findValue) {
    return middle;
  }

  if (array[middle] < findValue) {
    return binarySearch(array, findValue, middle + 1, end);
  }

  return binarySearch(array, findValue, start, middle - 1);
}

/**
 *
 * @param {Array} array
 * @param {number} findValue
 * @param {number} [start]
 * @param {number} [end]
 * @returns {number} index location within the array
 */
module.exports.binarySearch = binarySearch;
