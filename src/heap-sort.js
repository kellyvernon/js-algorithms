function heapSort(array) {
  return array;
}

function buildMaxHeap(array) {
  return array;
}

function buildHeap(array) {
  return array;
}

/**
 *
 * @param {*} value
 * @constructor
 */
function BinaryNode(value) {
  this.parent = null;
  this.left = null;
  this.right = null;

  /**
   * @type {*}
   */
  this.value = value;
}
BinaryNode.prototype.count = function () {

  /**
   *
   * @param {BinaryNode} node
   * @returns {number}
   */
  const counter = function (node) {
    let progressCount = 1;

    if (node.left === null && node.right === null) {
      return progressCount;
    }

    if (node.left !== null) {
      progressCount += counter(node.left);
    }

    if (node.right !== null) {
      progressCount += counter(node.right);
    }

    return progressCount;
  };

  return counter(this);
};
/**
 *
 * @param value
 * @param {BinaryNode} [parentNode]
 */
BinaryNode.prototype.add = function (value) {
  const addChild = function (value, parentNode) {
    const node = new BinaryNode(value);

    if (parentNode.value > value) {
      if (parentNode.left === null) {
        parentNode.left = node;
        return;
      }

      addChild(value, parentNode.left);
    }

    if (parentNode.value < value || parentNode.value === value) {
      if (parentNode.right === null) {
        parentNode.right = node;
        return;
      }

      addChild(value, parentNode.right);
    }
  };

  addChild(value, this);
};
/**
 *
 * @param {function(Node)} callback
 */
BinaryNode.prototype.iterate = function (callback) {
  const loop = function (node) {
    callback(node);

    if (node.left === null && node.right === null) {
      return;
    }

    if (node.left !== null) loop(node.left);
    if (node.right !== null) loop(node.right);
  };

  loop(this);
};

/**
 *
 * @param {*} value
 * @constructor
 */
function Node(value) {
  this.parent = null;
  this.children = [];

  /**
   * @type {*}
   */
  this.value = value;
}
Node.prototype.add = function (value) {
  const node = new Node(value);
  node.parent = this;
  this.children.push(node);
};
/**
 *
 * @param {function(Node)} callback
 */
Node.prototype.iterate = function (callback) {
  const loop = function (node) {
    callback(node);

    if (node.children.length === 0) {
      return;
    }

    for (let i = 0; i < node.children.length; i++) {
      loop(node.children[i]);
    }
  };

  loop(this);
};
Node.prototype.count = function () {

  /**
   *
   * @param {Node} node
   * @returns {number}
   */
  const counter = function (node) {
    if (node.children.length === 0) {
      return 0;
    }

    let progressCount = node.children.length;

    for (let i = 0; i < node.children.length; i++) {
      progressCount += counter(node.children[i]);
    }

    return progressCount;
  };

  return counter(this);
};

/**
 *
 * @param {Node|BinaryNode} [type=Node] based on type
 * @constructor
 */
function Tree(type) {
  this.type = type === null || type === undefined
    ? Node
    : type;
}
Tree.prototype.root = function () {
  if (!!this._root === false) {
    return null;
  }

  return this._root;
};
Tree.prototype.count = function () {
  return this.root().count();
};
/**
 *
 * @param {*} value
 * @param {Node} [parentNode]
 */
Tree.prototype.add = function (value, parentNode) {
  if (!this.root()) {
    this._root = new this.type(value);
    return;
  }

  if (!!parentNode === false) {
    this.root().add(value);
  } else {
    parentNode.add(value);
  }
};
/**
 *
 * @param {Array} values
 */
Tree.prototype.addRange = function (values) {
  if (!values || values.length === 0) {
    return;
  }

  for (let i = 0; i < values.length; i++) {
    const value = values[i];
    console.log('addRange', value, this.root());
    this.add(value, this.root());
  }
};
Tree.prototype.iterate = function (callback) {
  this.root().iterate(callback);
};

module.exports.heapSort = heapSort;
module.exports.buildHeap = buildHeap;
module.exports.buildMaxHeap = buildMaxHeap;
module.exports.Node = Node;
module.exports.BinaryNode = BinaryNode;
module.exports.Tree = Tree;
