'use strict';

import {expect} from 'chai';
import {binarySearch} from './../../src/binary-search';

const randomNumber = value => Math.floor(Math.random() * value);

describe('binary search tests', function () {
  it('should get first number', () => {
    const array = [2, 3, 4, 5, 6, 7, 8];

    expect(binarySearch(array, array[0])).to.equal(0);
  });

  it('should get last number', () => {
    const array = [2, 3, 4, 5, 6, 7, 8];

    expect(binarySearch(array, array[array.length - 1])).to.equal(array.length - 1);
  });

  it('should first half random number', () => {
    const array = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    const randomIndex = randomNumber(array.length / 2) + 1;

    const findValue = array[randomIndex];

    expect(binarySearch(array, findValue)).to.equal(randomIndex);
  });

  it('should second half random number', () => {
    const array = [8, 9, 10, 11, 12, 13, 14];

    const halfAmount = Math.floor(array.length / 2);

    const rand = randomNumber(halfAmount);

    const randomIndex = rand + halfAmount;

    const findValue = array[randomIndex];

    expect(binarySearch(array, findValue)).to.equal(randomIndex);
  });
});
