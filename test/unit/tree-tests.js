'use strict';

import {expect} from 'chai';

import {Tree, Node} from './../../src/heap-sort';
import {BinaryNode} from "../../src/heap-sort";

describe('tree tests', function () {
  it('create', function () {
    const tree = new Tree(Node);

    expect(tree).to.not.be.null;
  });

  it('should add child to parent', function () {
    const tree = new Tree(Node);
    tree.add(3);

    const expectedNode = new Node(20);
    expectedNode.parent = tree.root();

    tree.add(expectedNode.value, tree.root());

    expect([expectedNode]).to.deep.equal(tree.root().children);
  });

  it('should get node count', function () {
    const tree = new Tree(Node);
    tree.add(3);
    expect(0).to.equal(tree.count());
  });

  it('should get node count', function () {
    const tree = new Tree(Node);
    tree.add(0);
    tree.add(1, tree.root());
    tree.add(2, tree.root());

    expect(2).to.equal(tree.count());
  });

  it('should only add 2 nodes', function () {
    const tree = new Tree(BinaryNode);
    tree.add(0);
    tree.add(1);
    tree.add(2);
  });

  it('should get node count of chain', function () {
    const tree = new Tree(Node);
    tree.add(0);
    tree.add(1, tree.root());
    tree.add(2, tree.root());
    tree.add(3, tree.root().children[0]);

    expect(3).to.equal(tree.count());
  });

  it('should write out chain', function () {
    const tree = new Tree(Node);
    tree.add(0);
    tree.add(1, tree.root());
    tree.add(2, tree.root());
    tree.add(3, tree.root().children[0]);

    let actualCount = 0;
    const counter = function (node) {
      actualCount++;
      console.log(node.value);
    };

    tree.iterate(counter);
    expect(4).to.equal(actualCount);
  });

  it('should build from array', function () {
    const array = [1, 4, 2, 3, 6, 10, 9];

    const tree = new Tree(BinaryNode);

    tree.addRange(array);

    tree.iterate((n) => console.log(n.value));

    expect(array.length).to.equal(tree.count());
  });
});
