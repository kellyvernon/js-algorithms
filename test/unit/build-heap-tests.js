'use strict';

import {expect} from 'chai';
import {buildHeap} from './../../src/heap-sort';

xdescribe('build heap sort tests', function () {
  let ary, expectedResult;

  beforeEach(function () {
    ary = [6, 5, 3, 1, 2, 4, 7];
    expectedResult = ary.slice().sort();
  });

  afterEach(function () {
    ary = null;
  });

  it('should return same list', function () {
    const undefinedArray = undefined;
    const result = buildHeap(undefinedArray);

    expect(result).to.equal(undefinedArray);
  });

  it('should return same list', function () {
    const emptyArray = [];
    const result = buildHeap(emptyArray);

    expect(result).to.deep.equal(emptyArray);
  });

  it('should return sorted list', function () {
    const result = buildHeap(ary);

    expect(expectedResult).to.deep.equal(result);
  });
});
