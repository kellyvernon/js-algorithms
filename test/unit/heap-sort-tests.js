'use strict';

import {expect} from 'chai';
import {heapSort} from './../../src/heap-sort';

xdescribe('heap sort tests tests', function () {
  let ary, expectedResult;

  beforeEach(function () {
    ary = [6, 5, 3, 1, 2, 4, 7];
    expectedResult = ary.slice().sort();
  });

  afterEach(function () {
    ary = null;
  });

  it('should return same list', function () {
    const undefinedArray = undefined;
    const result = heapSort(undefinedArray);

    expect(result).to.equal(undefinedArray);
  });

  it('should return same list', function () {
    const emptyArray = [];
    const result = heapSort(emptyArray);

    expect(result).to.deep.equal(emptyArray);
  });

  it('should return sorted list', function () {
    const result = heapSort(ary);

    expect(expectedResult).to.deep.equal(result);
  });
});
