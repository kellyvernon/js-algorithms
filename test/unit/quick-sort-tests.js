'use strict';

import {expect} from 'chai';
import {quickSort} from './../../src/quick-sort';

describe('quick sort tests tests', function () {
  let ary, expectedResult;

  beforeEach(function () {
    ary = [6, 5, 3, 1, 2, 4, 7];
    expectedResult = ary.slice().sort();
  });

  afterEach(function () {
    ary = null;
  });

  it('should return list', function () {
    const undefinedArray = undefined;
    const result = quickSort(undefinedArray);

    expect(result).to.equal(undefinedArray);
  });

  it('should return list', function () {
    const undefinedArray = undefined;
    const result = quickSort(undefinedArray);

    expect(result).to.equal(undefinedArray);
  });

  it('should return sorted list', function () {
    const result = quickSort(ary);

    expect(expectedResult).to.deep.equal(result);
  });
});
