'use strict';

import {expect} from 'chai';
import {buildMaxHeap} from './../../src/heap-sort';

xdescribe('build max heap tests', function () {
  let ary, expectedResult;

  beforeEach(function () {
    ary = [6, 5, 3, 1, 2, 4, 7];
    expectedResult = ary.slice().sort();
  });

  afterEach(function () {
    ary = null;
  });

  it('should return same list', function () {
    const undefinedArray = undefined;
    const result = buildMaxHeap(undefinedArray);

    expect(result).to.equal(undefinedArray);
  });

  it('should return same list', function () {
    const emptyArray = [];
    const result = buildMaxHeap(emptyArray);

    expect(result).to.deep.equal(emptyArray);
  });

  it('should return sorted list', function () {
    const result = buildMaxHeap(ary);

    expect(expectedResult).to.deep.equal(result);
  });
});
