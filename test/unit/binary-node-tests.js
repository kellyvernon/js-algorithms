'use strict';

import {expect} from 'chai';

import {BinaryNode} from "../../src/heap-sort";

describe('binary node tests', function () {
  it('should create', function () {
    const b = new BinaryNode(1);

    expect(b.value).to.equal(1);
  });

  it('adding with less', function () {
    const b = new BinaryNode(1);
    b.add(0);
    expect(b.left.value).to.equal(0);
  });

  it('adding with more', function () {
    const b = new BinaryNode(1);
    b.add(2);
    expect(b.right.value).to.equal(2);
  });

  it('adding with less', function () {
    const b = new BinaryNode(10);
    b.add(8);
    b.add(7);
    expect(b.left.left.value).to.equal(7);
  });

  it('adding with less then more', function () {
    const b = new BinaryNode(10);
    b.add(8);
    b.add(9);
    expect(b.left.right.value).to.equal(9);
    expect(b.count()).to.equal(3);
  });

  it('adding with more', function () {
    const b = new BinaryNode(10);
    b.add(8);
    b.add(12);
    b.add(13);
    b.add(13);
    expect(b.right.right.value).to.equal(13);
    expect(b.count()).to.equal(5);
  });
});
